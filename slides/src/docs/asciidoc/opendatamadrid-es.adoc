= OpenSource OpenData
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2018-09-01
:revnumber: {project-version}
:example-caption!:
ifndef::imagesdir[:imagesdir: images]
ifndef::sourcedir[:sourcedir: ../java]


++++
<style>
.grid-none td{
  border-bottom: none;
}
.grid-none a{
  font-size: 0.6em;
}

</style>
++++

include::fragments/whoIam.adoc[]

== Agenda

[%step]

- Qué es (y qué no es) esta charla
- Buzzwords
- OpenSource y OpenData
- Cámaras de tráfico. De querer probar Kafka a publicar 3 Bots
- Carreras populares y poyaques: canal de Telegram
- BigQuery DataSet Préstamos
- Spin-off: gif-generator, import-csv, social network plugin

== Qué es (y qué no es) esta charla

- Charla práctica poco académica
- Catálogo de datos abiertos de Madrid y otros
- Aplicaciones OpenSource, NO comerciales
- Formatos usados
- Tal vez sean "inspiradoras"

== Buzzwords

- html, csv, json, xls, kml
- Java y Groovy como lenguajes principales
- Grails y Micronaut como frameworks
- Gradle como herramienta construcción
- Telegram, Gitlab, Pipeline Digital Ocean y Google AppEngine como infraestructura

== OpenSource OpenData

- Open Source: modelo de desarrollo de software basado en la colaboración abierta
enfocado más a lo práctico que a lo ético (Wikipedia)

- Open Data: filosofía y práctica para ofrecer determinados datos a todo el mundo sin restricciones
de derechos de autor, patentes, etc (Otra vez Wikipedia)

== Cámaras de tráfico

(probando Kafka)

[%step]

- Xml, Jpg y Png
- Catálogo de datos
* https://datos.madrid.es/portal/site/egob/[window="_blank"]
* http://www.mc30.es/components/com_hotspots/datos/camaras.xml[window="_blank"]
- Projecto M30Gif https://jorge-aguilera.gitlab.io/m30gif/[window="_blank"]

=== Demo

[source]
----
docker-compose up -d
http://localhost:8080
----

NOTE: Estuvo desplegada en DigitalOcean un par de meses (5-10€ al mes) con SSL gracias
a *Let's Encrypt*

== Cámaras trafico bot

M30Bot, el spin-off de m30gif

- Bot de Telegram que recibe peticiones de calles y devuelve
la última imagen (gif animado )disponible
- https://gitlab.com/jorge-aguilera/grails-m30-bot

=== Cámaras trafico bot, Madrid

- Grails 4 corriendo en capa gratuita Google AppEngine (startup time < 3seg)

- catálogo de datos
* https://datos.madrid.es/portal/site/egob/[window="_blank"]
* http://www.dgt.es/es/el-trafico/camaras-de-trafico/[window="_blank"]

=== Demo

https://web.telegram.org/#/im?p=@M30Madridbot[window="_blank"]

=== ..., Barcelona, Granada

- catálogo de datos
* https://opendata-ajuntament.barcelona.cat/[window="_blank"]
* http://www.movilidadgranada.com/ + https://sierranevada.es[window="_blank"]
* Webcams Bariloche

=== Demo

https://web.telegram.org/#/im?p=@TraficoGranadaBot[window="_blank"]

=== Spin-off Gif generator

- https://gitlab.com/puravida-software/gif-generator/
- Librería Java disponible en Maven Central (https://mvnrepository.com/artifact/com.puravida.gif/gif-generator)

== Carreras Madrid

pass:[...] ahora Eventos Madrid

- Mensaje diario a un canal de Telegram con los próximos eventos (carreras y actividades
de bibliotecas)
- Catálogo de datos
* https://datos.madrid.es/portal/site/egob/
* formatos Excel, Json

=== Arquitectura

- https://gitlab.com/jorge-aguilera/carreras-madrid[window="_blank"]
- Usando el scheduler pipeline de Gitlab ejecutamos diariamente un build de Gradle
- Desde Gitlab enviamos un resumen a un canal de Telegram

=== Tasks

https://gitlab.com/jorge-aguilera/carreras-madrid/raw/master/buildSrc/src/main/groovy/madrid/CarrerasTask.groovy[CarrerasTask.groovy,window="_blank"]

https://gitlab.com/jorge-aguilera/carreras-madrid/raw/master/buildSrc/src/main/groovy/madrid/EventosBibliotecasTask.groovy[EventosBibliotecasTask.groovy,window="_blank"]


=== Demo

https://web.telegram.org/#/im?p=@opendatamadrid[window="_blank"]


=== Spin-off Plugin SocialNetwork

- https://puravida-gradle.gitlab.io/social-network/[window="_blank"]

- Plugin Gradle para enviar mensajes a Telegram (más Twitter y Slack)


=== Préstamos

- Incluir los libros más leídos el mes anterior

[%step]
- Reto: parsear CSV con 200K líneas y buscar los TOP
- Solución: usar una bbdd Derby en local
- https://gitlab.com/jorge-aguilera/carreras-madrid/raw/master/build.gradle[task queryPrestamos,window="_blank"]


=== Spin-off QueryCSV Plugin

- https://puravida-gradle.gitlab.io/query-csv/[window="_blank"]

- Plugin Gradle para tratar CSV "pesados" y extraer información mediante SQL


== Bibliomadrid

Importar en BigQuery de Google todo el histórico de préstamos y crear un DataSet abierto
para su consulta

https://gitlab.com/jorge-aguilera/bibliomadrid[window="_blank"]

=== Ejemplos

tweet::Chuso_Jar[1128038304966901761]

=== Ejemplos

tweet::jagedn[1128049388822061056]

tweet::jagedn[1128044988384677888]

== European Data Portal

video::nKc840c3D_E[youtube,width=500,height=400]

== Conclusiones

* Infinidad de datos ahí fuera
* Oportunidad de aprender
* Coste "cero" (económico porque en tiempo ....)

:google-form: A3CkzVadYRdhBYXi8
include::fragments/end.adoc[]

