= Telegram (channels y bots)
Jorge Aguilera jorge.aguilera@puravida-software.com
2019-02-01
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
:idprefix:
:idseparator: -

Telegram es un servicio de mensajería que permite además crear canales públicos y/o privados donde
unos administradores pueden enviar mensajes multimedia que pueden ser leídos por los subscriptores.
Así mismo permite crear `bots` capaces de escribir en estos canales de forma autónoma o de "dialogar" con usuarios
ofreciendo servicios, utilidades, integraciones con servicios empresariales y un largo etcetera

Esta es una lista de alguno de los canales y bots desarrollados por Puravida Software para la comunidad (en orden cronológico):

== OpenDataMadrid

Un canal donde recibir diariamente notificaciones sobre eventos culturales de la ciudad de Madrid (carreras urbanas,
actividades de las bibliotecas, etc)

Enlace: https://t.me/opendatamadrid

== Actividades Barcelona

Un canal donde recibir diariamente notificaciones sobre eventos culturales de la ciudad de Barcelona

https://t.me/ActividadesEnBarcelona

== Actividades Granada

Un canal donde recibir diariamente notificaciones sobre eventos culturales de la ciudad de Granada

https://t.me/ActividadesGranada

== Cámaras de tráfico de Madrid

Un bot al que el usuario le puede pedir visualizar las cámaras de tráfico de la ciudad de Madrid y alrededores
(M30, calles de Madrid, M40, A5, A6)

Nombre: @M30MadridBot

== Cámaras de tráfico de Barcelona

Un bot al que el usuario le puede pedir visualizar las cámaras de tráfico de la ciudad de Barcelona y alrededores

Nombre: @TraficoBarcelonaBot

== Cámaras de tráfico de Granada

Un bot al que el usuario le puede pedir visualizar las cámaras de tráfico de la ciudad de Granada (y
las pistas de esquí de Sierra Nevada)

Nombre: @TraficoGranadaBot

== Fuentes públicas de beber de España

Un bot que al principio mostraba las fuentes públicas (de beber agua potable) de Madrid, ampliado posteriormente
a otras ciudades que publican estos datos como Barcelona y Cáceres. Así mismo un ciudadano de Granada ha geoposicionado
las fuentes de esta ciudad y se ha incluido en el bot

Nombre: @fuentesMadridBot

== Actividades en Madrid

Permite al usuario conocer las actividades que se van a realizar cerca de donde se encuentra (o la localización que
indique) hoy, mañana o durante la semana.

Nombre: @AtividadesMadridBot

== Precio de gasolineras España

Buscar la estación de servicio (gasolinera) más cercana y barata es posible enviando tu posición a este bot.
Permite además marcar una estación como favorita y ser notificado cuando esta cambie el precio con un mensaje informativo

Nombre: @EstacionesDeServicioBot

== Agenda WeCodeFest

Un bot para consultar la agenda del evento WeCodeFest. Cuenta con el reto de que la agenda no será definida hasta el
inicio del evento, en el que los participantes decidirán su composición

Nombre: @WeCodeFestBot

== Qué hora es en...

Un bot `inline`, es decir se puede usar en una conversación entre varios sin necesidad de dialogar en otro canal
únicamente con él, para saber la hora que es ahora mismo en cualquiera de las 20340 ciudades que acepta. Muestra
resultados de posibles ciudades según se va escribiendo el nombre de la ciudad para facilitar la búsqueda

Para usarlo, simplemente en cualquier conversacion empieza un mensaje con :

`@horaenbot madr`  y mostrará un menú con ciudades que contengan ese texto, como `Madrid`, `Rivasmadrid`

Nombre: @horaenbot

== Calcular el porcentaje de una cantidad

Un bot `inline`, es decir se puede usar en una conversación entre varios sin necesidad de dialogar en otro canal
únicamente con él, para saber el tanto por ciento de una cantidad. La lista de % a usar es una lista prefijada
con los más comunes.

Para usarlo, simplemente en cualquier conversacion empieza un mensaje con :

`@porcientobot 123`  y según vayas indicando la cantidad aparecerá un menú con diferentes porcentajes

Nombre: @porcientobot


== A cúanto tocamos

Un bot `inline`, es decir se puede usar en una conversación entre varios sin necesidad de dialogar en otro canal
únicamente con él, para repartir una cantidad entre varios participantes (la cuenta del restaurante entre los
comensales por ejemplo)

Para usarlo, simplemente en cualquier conversacion empieza un mensaje con :

`@tocamosbot 123`  y según vayas indicando la cantidad aparecerá un menú donde podrás indicar entre cuántos
hay que repartir esa cantidad. Algo sencillo de hacer con una calculadora pero de esta forma aparecerá en el chat
del grupo

Nombre: @tocamosbot





